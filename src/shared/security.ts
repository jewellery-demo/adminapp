let userInfo: IUserInfo | undefined;

export const setUser = (user: IUserInfo | undefined): void => {
    userInfo = user;
};

export const getUser = (): IUserInfo | undefined => {
    return userInfo;
};

export const isAuth = (): boolean => {
    return userInfo ? true : false;
};

export const isRoleAdmin = () => {
    return userInfo && userInfo.roles && userInfo.roles.indexOf('admin') >= 0;
};

export const isRoleGuest = () => {
    return userInfo && userInfo.roles && userInfo.roles.indexOf('guest') >= 0;
};
