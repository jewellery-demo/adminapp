export const random = (min: number, max: number) => {
  return Math.floor(Math.random() * (max - min + 1) + min);
};

/**
 * Randomize array element order in-place.
 * Using Durstenfeld shuffle algorithm.
 */
export const shuffleArray = (array:
  Array<{username: string, registered: string, role: string, status: string, _rowVariant?: string}>) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
  return array;
};

export enum EDateFormat {
  yyyyMMdd_slash,
  yyyyMMdd_dash,
  ddMMyyyy_slash,
}

export const formatDate = (date: Date, format: EDateFormat): string => {
  if (format === EDateFormat.ddMMyyyy_slash) {
    return appendZero(date.getDate()) + '/' + appendZero(date.getMonth() + 1) + '/' + date.getFullYear();
  } else if (format === EDateFormat.yyyyMMdd_slash) {
    return date.getFullYear() + '/' + appendZero(date.getMonth() + 1) + '/' + appendZero(date.getDate());
  } else if (format === EDateFormat.yyyyMMdd_dash) {
    return date.getFullYear() + '-' + appendZero(date.getMonth() + 1) + '-' + appendZero(date.getDate());
  } else {
    return '';
  }
};

export const getDataUri = (url: string) => new Promise((resolve) => {
  const image = new Image();
  image.onload = function() {
    const canvas = document.createElement('canvas');
    const imageTag = this as HTMLImageElement;
    canvas.width = imageTag.naturalWidth;
    canvas.height = imageTag.naturalHeight;
    canvas.getContext('2d')!.drawImage(imageTag, 0, 0);
    resolve(canvas.toDataURL('image/png'));
  };
  image.src = url;
});

const appendZero = (n: number): string => {
  if (n.toString().length < 2) {
    return '0' + n.toString();
  } else {
    return n.toString();
  }
};
