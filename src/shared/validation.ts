import { Component } from 'vue-property-decorator';
import { validationMixin } from 'vuelidate';
import { Vue } from 'vue-property-decorator';

@Component({
    mixins: [validationMixin],
})
export class Validation extends Vue {
    protected isLoading: boolean = false;

    get disabledCustom(): boolean {
        return false;
    }

    get submitButtonDisabled(): boolean {
        if (this.isLoading) {
            return true;
        } else if (this.$v && !this.$v.$anyDirty) {
            return true;
        } else if (this.$v && this.$v.$anyDirty) {
            return false;
        } else {
            return true;
        }
    }

    get submitButtonStyle(): string {
        if (this.isLoading) {
            return 'buttonDisabled';
        } else if (this.$v && !this.$v.$invalid && !this.disabledCustom) {
            return '';
        } else {
            return 'buttonDisabled';
        }
    }

    get validationClass() {
        return (vElement: IVElement, extra?: IExtra) => {
            if (extra && extra.stopValidation) {
                return '';
            } else if (vElement && vElement.$dirty) {
                return (this.validationState(vElement, extra) && (!extra || !extra.customMessage)) ?
                    'is-valid' : 'is-invalid';
            } else if (extra && extra.customMessage) {
                return 'is-invalid';
            } else {
                return '';
            }
        };
    }

    get validationState() {
        return (vElement: IVElement, extra?: IExtra) => {
            if (extra && extra.stopValidation) {
                return true;
            } else if (vElement && vElement.$dirty) {
                let result: boolean = true;
                for (const key in vElement) {
                    if (!key.startsWith('$') && !vElement[key]) {
                        result = false;
                        break;
                    }
                }

                return result && (!extra || !extra.customMessage);
            } else if (extra && extra.customMessage) {
                return false;
            } else {
                return '';
            }
        };
    }

    get validationFeedback() {
        return (vElement: IVElement, message: IMessage, extra?: IExtra) => {
            if (extra && extra.customMessage) {
                return extra.customMessage;
            } else if (vElement && vElement.$dirty) {
                for (const key in vElement) {
                    if (!key.startsWith('$') && !vElement[key]) {
                        return message[key];
                    }
                }
            }

            return '';
        };
    }

    protected keyUp(keyUpFunction: () => void) {
        keyUpFunction();
        this.isLoading = false;
    }
}

export const isPhone = (value: string) => {
    return /^[1-9]\d{10,11}$/.test(value);
};

export const isCurrency = (value: string) => {
    return /^[0-9]*(\.[0-9]{1,2})?$/.test(value);
};

export const isDate = (value: number) => {
    if (isNaN(value) || value < 1 || value > 31) {
        return false;
    } else {
        return true;
    }
};

export const isPercentage = (value: string) => {
    return /^[0-9]*(\.[0-9]+)?$/.test(value);
};

export const isGreaterThan = (threshold: number) => {
    return (value: number) => value > threshold ? true : false;
};

export const isLessThan = (threshold: number) => {
    return (value: number) => value < threshold ? true : false;
};
