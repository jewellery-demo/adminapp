interface IExtra {
    stopValidation?: boolean,
    customMessage?: string
}

interface IMessage {
    [key: string]: string
}

interface IVElement {
    [key: string]: any,
    $dirty: boolean
}
