interface IRouterRecord {
    meta: {
        auth?: boolean,
        anonymous?: boolean,
        roles?: string[],
    }
}

interface IUserInfo {
    email: string,
    type: string,
    roles: string[],
    createdDate: Date
}
