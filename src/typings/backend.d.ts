import { EReportDisclosure, EReportSignarure } from "@/typings/enum/store.enum";

interface IProductCategoryRequest {
    label: string,
    code: string
}

interface IProductTypeRequest {
    label: string,
    code: string
}

interface IReportLabelRequest {
    label: string
}

interface IReportFieldRequest {
    reportLabelId?: string | null,
    reportValue?: string | null
}

interface IReportRequest {
    name: string,
    photo: string,
    disclosure: EReportDisclosure,
    signatures: EReportSignarure[],
    productCategoryId: string,
    productTypeId: string,
    reportFields: IReportFieldRequest[]
}
