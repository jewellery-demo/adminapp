export enum EReportDisclosure {
    general = 'general',
    diamond = 'diamond',
}

export enum EReportSignarure {
    seniorIGS = 'seniorGemologistIGS',
    seniorGIA = 'seniorGemologistGIA',
    chiefGIA = 'chiefGemologistGIA',
}
