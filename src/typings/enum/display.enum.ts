export enum EProductCategoryKeyLabel {
    code = 'Code',
    label = 'Label',
    createdAt = 'Created At',
    updatedAt = 'Updated At',
}

export enum EProductTypeKeyLabel {
    code = 'Code',
    label = 'Label',
    createdAt = 'Created At',
    updatedAt = 'Updated At',
}

export enum EReportLabelKeyLabel {
    label = 'Label',
    createdAt = 'Created At',
    updatedAt = 'Updated At',
}

export enum EReportKeyLabel {
    name = 'Report Name',
    photo = 'Item Photo',
    disclosure = 'Report Disclosure',
    signatures = 'Report Signatures',
    productCategory = 'Product Category',
    productType = 'Product Type',
    reportFields = 'Fields',
    createdAt = 'Created At',
    updatedAt = 'Updated At',
}
