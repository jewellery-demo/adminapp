export enum EBackendPath {
    productCategories = 'products/categories',
    productTypes = 'products/types',
    reportLabels = 'reports/labels',
    reports = 'reports',
}
