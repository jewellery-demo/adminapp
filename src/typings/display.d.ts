import { IReportFieldRequest } from "@/typings/backend";

interface IField {
    key: string
}

interface IFieldLabel {
    key: string
    label: string
}

interface IUserData {
    id: number,
    name: string,
    registered: string,
    role: string,
    status: string
}

interface IReportFieldRequestDisplay extends IReportFieldRequest{
    id: number
}
