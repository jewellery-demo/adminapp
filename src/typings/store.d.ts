import { EReportDisclosure, EReportSignarure } from "@/typings/enum/store.enum";

interface IRootState {}

interface IState<T> {
    list: T[],
    isLoad: boolean
}

interface IUpdate<T> {
    id: string,
    data: T,
    successCb?: () => void,
    errorCb?: (error: string) => void
}

interface ICreate<T> {
    data: T,
    successCb?: (id?: string) => void,
    errorCb?: (error: string) => void
}

interface IDelete {
    id: string,
    successCb?: () => void,
    errorCb?: (error: string) => void
}

interface IProductCategory {
    id: string,
    label: string,
    code: string,
    createdAt: number,
    updatedAt: number
}

interface IProductType {
    id: string,
    label: string,
    code: string,
    createdAt: number,
    updatedAt: number
}

interface IReportLabel {
    id: string,
    label: string,
    createdAt: number,
    updatedAt: number
}

interface IReportField {
    reportLabel: IReportLabel,
    reportValue: string,
    sequence: number,
    isEmpty: boolean
}

interface IReport {
    id: string,
    reportId: string,
    name: string,
    photo: string,
    disclosure: EReportDisclosure,
    signatures: EReportSignarure[],
    productCategory: IProductCategory,
    productType: IProductType,
    reportFields: IReportField[],
    createdAt: number,
    updatedAt: number
}

interface ISearchCount {
    id: string,
    date: number,
    count: number
}
