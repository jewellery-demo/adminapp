import {MutationTree} from 'vuex';
import { ISearchCount, IState } from '@/typings/store';

let timeout: number;
export const mutations: MutationTree<IState<ISearchCount>> = {
    setSearchCounts(state: IState<ISearchCount>, data: ISearchCount[]) {
        state.list = [...data];
    },
    appendSearchCounts(state: IState<ISearchCount>, data: ISearchCount[]) {
        const addList = data.filter((count) => !state.list
            .some((stateCount) => stateCount.id === count.id));
        state.list = [...state.list, ...addList];
    },
    setSearchCountsLoad(state: IState<ISearchCount>, isLoad: boolean) {
        state.isLoad = isLoad;
        if (state.isLoad) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(() => {
                state.isLoad = false;
            }, 60000);
        }
    },
    updateSearchCount(state: IState<ISearchCount>, data: ISearchCount) {
        if (state.list.some((count) => count.id === data.id)) {
            state.list = state.list.map((count) => {
                if (count.id === data.id) {
                    count.date = data.date;
                    count.count = data.count;
                }
                return count;
            });
        } else {
            state.list.push({...data});
        }
    },
};
