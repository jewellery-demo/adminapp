import {ActionTree} from 'vuex';
import { IRootState, ISearchCount, IState } from '@/typings/store';
import { SearchCountsApi } from '@/backend/SearchCounts';

const API = new SearchCountsApi();

export const actions: ActionTree<IState<ISearchCount>, IRootState> = {
    async loadSearchCounts({commit, state}) {
        if (!state.isLoad) {
            const searchCounts = await API.list();
            if (searchCounts && searchCounts.length > 0) {
                if (state && state.list.length > 0) {
                    commit('appendSearchCounts', searchCounts);
                } else {
                    commit('setSearchCounts', searchCounts);
                }

                commit('setSearchCountsLoad', true);
            } else {
                commit('setSearchCountsLoad', true);
            }
        }
    },
    async addCount({commit}) {
        try {
            const searchCount = await API.addCount();
            if (searchCount) {
                commit('updateSearchCount', searchCount);
            }
        } catch (error) {
            console.error(error);
        }
    },
};
