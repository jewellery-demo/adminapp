import {Module} from 'vuex';
import {getters} from '@/store/searchCounts/getters';
import {actions} from '@/store/searchCounts/actions';
import {mutations} from '@/store/searchCounts/mutations';
import { IRootState, ISearchCount, IState } from '@/typings/store';

export const state: IState<ISearchCount> = {
    list: [],
    isLoad: false,
};

const searchCounts: Module<IState<ISearchCount>, IRootState> = {
    state,
    getters,
    actions,
    mutations,
};

export default searchCounts;
