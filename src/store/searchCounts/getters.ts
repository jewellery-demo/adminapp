import {GetterTree} from 'vuex';
import { IRootState, ISearchCount, IState } from '@/typings/store';

export const getters: GetterTree<IState<ISearchCount>, IRootState> = {
    searchCounts(state): ISearchCount[] {
        return [...state.list];
    },
    searchCount(state): (id: string) => ISearchCount | undefined {
        return (id: string) => state.list.find((c) => c.id === id);
    },
    isSearchCountsLoad(state): boolean {
        return state.isLoad;
    },
};
