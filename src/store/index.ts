import Vue from 'vue';
import Vuex from 'vuex';

import productCategories from './productCategories';
import productTypes from './productTypes';
import reportLabels from './reportLabels';
import reports from './reports';
import searchCounts from './searchCounts';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  modules: {
    productCategories,
    productTypes,
    reportLabels,
    reports,
    searchCounts,
  },
});
