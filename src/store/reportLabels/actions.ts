import {ActionTree} from 'vuex';
import {CrudApi} from '@/backend/Crud';
import {EBackendPath} from '@/typings/enum/backend.enum';
import { ICreate, IDelete, IReportLabel, IRootState, IState, IUpdate } from '@/typings/store';
import { IReportLabelRequest } from '@/typings/backend';

const ReportLabelApi = new CrudApi<IReportLabelRequest, IReportLabel>(EBackendPath.reportLabels);

export const actions: ActionTree<IState<IReportLabel>, IRootState> = {
    async loadReportLabels({commit, state}) {
        if (!state.isLoad) {
            const reportLabels = await ReportLabelApi.list();
            if (reportLabels && reportLabels.length > 0) {
                if (state && state.list.length > 0) {
                    commit('appendReportLabels', reportLabels);
                } else {
                    commit('setReportLabels', reportLabels);
                }

                commit('setReportLabelsLoad', true);
            } else {
                commit('setReportLabelsLoad', true);
            }
        }
    },
    async createReportLabel({commit}, request: ICreate<IReportLabelRequest>) {
        try {
            const reportLabel = await ReportLabelApi.save(request.data);
            commit('updateReportLabel', reportLabel);
            if (request.successCb) {
                request.successCb();
            }
        } catch (error) {
            console.error(error);
            if (request.errorCb) {
                request.errorCb(error.message);
            }
        }
    },
    async updateReportLabel({commit}, request: IUpdate<IReportLabelRequest>) {
        try {
            const reportLabel = await ReportLabelApi.update(request.id, request.data);
            commit('updateReportLabel', reportLabel);
            if (request.successCb) {
                request.successCb();
            }
        } catch (error) {
            console.error(error);
            if (request.errorCb) {
                request.errorCb(error.message);
            }
        }
    },
    async deleteReportLabel({commit}, request: IDelete) {
        try {
            const success = await ReportLabelApi.delete(request.id);
            if (success) {
                commit('deleteReportLabel', request.id);
                if (request.successCb) {
                    request.successCb();
                }
            } else {
                if (request.errorCb) {
                    request.errorCb('Error calling API');
                }
            }
        } catch (error) {
            console.error(error);
            if (request.errorCb) {
                request.errorCb(error.message);
            }
        }
    },
};
