import {GetterTree} from 'vuex';
import { IReportLabel, IRootState, IState } from '@/typings/store';

export const getters: GetterTree<IState<IReportLabel>, IRootState> = {
    reportLabels(state): IReportLabel[] {
        return [...state.list];
    },
    reportLabel(state): (id: string) => IReportLabel | undefined {
        return (id: string) => state.list.find((rl) => rl.id === id);
    },
    isReportLabelsLoad(state): boolean {
        return state.isLoad;
    },
};
