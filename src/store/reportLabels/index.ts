import {Module} from 'vuex';
import {getters} from '@/store/reportLabels/getters';
import {actions} from '@/store/reportLabels/actions';
import {mutations} from '@/store/reportLabels/mutations';
import { IReportLabel, IRootState, IState } from '@/typings/store';

export const state: IState<IReportLabel> = {
    list: [],
    isLoad: false,
};

const reportLabels: Module<IState<IReportLabel>, IRootState> = {
    state,
    getters,
    actions,
    mutations,
};

export default reportLabels;
