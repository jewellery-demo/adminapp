import {MutationTree} from 'vuex';
import { IReportLabel, IState } from '@/typings/store';

let timeout: number;
export const mutations: MutationTree<IState<IReportLabel>> = {
    setReportLabels(state: IState<IReportLabel>, data: IReportLabel[]) {
        state.list = [...data];
    },
    appendReportLabels(state: IState<IReportLabel>, data: IReportLabel[]) {
        const addList = data.filter((reportLabel) => !state.list
            .some((stateReportLabel) => stateReportLabel.id === reportLabel.id));
        state.list = [...state.list, ...addList];
    },
    setReportLabelsLoad(state: IState<IReportLabel>, isLoad: boolean) {
        state.isLoad = isLoad;
        if (state.isLoad) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(() => {
                state.isLoad = false;
            }, 60000);
        }
    },
    updateReportLabel(state: IState<IReportLabel>, data: IReportLabel) {
        if (state.list.some((reportLabel) => reportLabel.id === data.id)) {
            state.list = state.list.map((reportLabel) => {
                if (reportLabel.id === data.id) {
                    reportLabel.label = data.label;
                    reportLabel.createdAt = data.createdAt;
                    reportLabel.updatedAt = data.updatedAt;
                }
                return reportLabel;
            });
        } else {
            state.list.push({...data});
        }
    },
    deleteReportLabel(state: IState<IReportLabel>, id: string) {
        state.list = state.list.filter((reportLabel) => reportLabel.id !== id);
    },
};
