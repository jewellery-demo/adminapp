import {ActionTree} from 'vuex';
import {CrudApi} from '@/backend/Crud';
import {EBackendPath} from '@/typings/enum/backend.enum';
import { ICreate, IDelete, IReport, IRootState, IState, IUpdate } from '@/typings/store';
import { IReportRequest } from '@/typings/backend';

const ReportApi = new CrudApi<IReportRequest, IReport>(EBackendPath.reports);

export const actions: ActionTree<IState<IReport>, IRootState> = {
    async loadReports({commit, state}) {
        if (!state.isLoad) {
            const reports = await ReportApi.list();
            if (reports && reports.length > 0) {
                if (state && state.list.length > 0) {
                    commit('appendReports', reports);
                } else {
                    commit('setReports', reports);
                }

                commit('setReportsLoad', true);
            } else {
                commit('setReportsLoad', true);
            }
        }
    },
    async createReport({commit}, request: ICreate<IReportRequest>) {
        try {
            const report = await ReportApi.save(request.data);
            commit('updateReport', report);
            if (request.successCb) {
                request.successCb(report ? report.id : undefined);
            }
        } catch (error) {
            console.error(error);
            if (request.errorCb) {
                request.errorCb(error.message);
            }
        }
    },
    async updateReport({commit}, request: IUpdate<IReportRequest>) {
        try {
            const report = await ReportApi.update(request.id, request.data);
            commit('updateReport', report);
            if (request.successCb) {
                request.successCb();
            }
        } catch (error) {
            console.error(error);
            if (request.errorCb) {
                request.errorCb(error.message);
            }
        }
    },
    async deleteReport({commit}, request: IDelete) {
        try {
            const success = await ReportApi.delete(request.id);
            if (success) {
                commit('deleteReport', request.id);
                if (request.successCb) {
                    request.successCb();
                }
            } else {
                if (request.errorCb) {
                    request.errorCb('Error calling API');
                }
            }
        } catch (error) {
            console.error(error);
            if (request.errorCb) {
                request.errorCb(error.message);
            }
        }
    },
};
