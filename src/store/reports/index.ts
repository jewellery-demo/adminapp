import {Module} from 'vuex';
import {getters} from '@/store/reports/getters';
import {actions} from '@/store/reports/actions';
import {mutations} from '@/store/reports/mutations';
import { IReport, IRootState, IState } from '@/typings/store';

export const state: IState<IReport> = {
    list: [],
    isLoad: false,
};

const reports: Module<IState<IReport>, IRootState> = {
    state,
    getters,
    actions,
    mutations,
};

export default reports;
