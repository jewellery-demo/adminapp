import {GetterTree} from 'vuex';
import { IReport, IRootState, IState } from '@/typings/store';

export const getters: GetterTree<IState<IReport>, IRootState> = {
    reports(state): IReport[] {
        return [...state.list];
    },
    report(state): (id: string) => IReport | undefined {
        return (id: string) => state.list.find((r) => r.id === id);
    },
    isReportsLoad(state): boolean {
        return state.isLoad;
    },
};
