import {MutationTree} from 'vuex';
import { IReport, IState } from '@/typings/store';

let timeout: number;
export const mutations: MutationTree<IState<IReport>> = {
    setReports(state: IState<IReport>, data: IReport[]) {
        state.list = [...data];
    },
    appendReports(state: IState<IReport>, data: IReport[]) {
        const addList = data.filter((report) => !state.list
            .some((stateReport) => stateReport.id === report.id));
        state.list = [...state.list, ...addList];
    },
    setReportsLoad(state: IState<IReport>, isLoad: boolean) {
        state.isLoad = isLoad;
        if (state.isLoad) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(() => {
                state.isLoad = false;
            }, 60000);
        }
    },
    updateReport(state: IState<IReport>, data: IReport) {
        if (state.list.some((report) => report.id === data.id)) {
            state.list = state.list.map((report) => {
                if (report.id === data.id) {
                    report.reportId = data.reportId;
                    report.name = data.name;
                    report.photo = data.photo;
                    report.disclosure = data.disclosure;
                    report.signatures = data.signatures;
                    report.productCategory = {...data.productCategory};
                    report.productType = {...data.productType};
                    report.reportFields = [...data.reportFields];
                    report.createdAt = data.createdAt;
                    report.updatedAt = data.updatedAt;
                }
                return report;
            });
        } else {
            state.list.push({...data});
        }
    },
    deleteReport(state: IState<IReport>, id: string) {
        state.list = state.list.filter((report) => report.id !== id);
    },
};
