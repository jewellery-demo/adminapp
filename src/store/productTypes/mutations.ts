import {MutationTree} from 'vuex';
import { IProductType, IState } from '@/typings/store';

let timeout: number;
export const mutations: MutationTree<IState<IProductType>> = {
    setProductTypes(state: IState<IProductType>, data: IProductType[]) {
        state.list = [...data];
    },
    appendProductTypes(state: IState<IProductType>, data: IProductType[]) {
        const addList = data.filter((type) => !state.list
            .some((stateType) => stateType.id === type.id));
        state.list = [...state.list, ...addList];
    },
    setProductTypesLoad(state: IState<IProductType>, isLoad: boolean) {
        state.isLoad = isLoad;
        if (state.isLoad) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(() => {
                state.isLoad = false;
            }, 60000);
        }
    },
    updateProductType(state: IState<IProductType>, data: IProductType) {
        if (state.list.some((type) => type.id === data.id)) {
            state.list = state.list.map((type) => {
                if (type.id === data.id) {
                    type.label = data.label;
                    type.code = data.code;
                    type.createdAt = data.createdAt;
                    type.updatedAt = data.updatedAt;
                }
                return type;
            });
        } else {
            state.list.push({...data});
        }
    },
    deleteProductType(state: IState<IProductType>, id: string) {
        state.list = state.list.filter((type) => type.id !== id);
    },
};
