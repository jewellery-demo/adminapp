import {Module} from 'vuex';
import {getters} from '@/store/productTypes/getters';
import {actions} from '@/store/productTypes/actions';
import {mutations} from '@/store/productTypes/mutations';
import { IProductType, IRootState, IState } from '@/typings/store';

export const state: IState<IProductType> = {
    list: [],
    isLoad: false,
};

const productTypes: Module<IState<IProductType>, IRootState> = {
    state,
    getters,
    actions,
    mutations,
};

export default productTypes;
