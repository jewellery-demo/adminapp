import {GetterTree} from 'vuex';
import { IProductType, IRootState, IState } from '@/typings/store';

export const getters: GetterTree<IState<IProductType>, IRootState> = {
    productTypes(state): IProductType[] {
        return [...state.list];
    },
    productType(state): (id: string) => IProductType | undefined {
        return (id: string) => state.list.find((t) => t.id === id);
    },
    isProductTypesLoad(state): boolean {
        return state.isLoad;
    },
};
