import {ActionTree} from 'vuex';
import {CrudApi} from '@/backend/Crud';
import {EBackendPath} from '@/typings/enum/backend.enum';
import { ICreate, IDelete, IProductType, IRootState, IState, IUpdate } from '@/typings/store';
import { IProductTypeRequest } from '@/typings/backend';

const ProductTypeApi = new CrudApi<IProductTypeRequest, IProductType>(EBackendPath.productTypes);

export const actions: ActionTree<IState<IProductType>, IRootState> = {
    async loadProductTypes({commit, state}) {
        if (!state.isLoad) {
            const productTypes = await ProductTypeApi.list();
            if (productTypes && productTypes.length > 0) {
                if (state && state.list.length > 0) {
                    commit('appendProductTypes', productTypes);
                } else {
                    commit('setProductTypes', productTypes);
                }

                commit('setProductTypesLoad', true);
            } else {
                commit('setProductTypesLoad', true);
            }
        }
    },
    async createProductType({commit}, request: ICreate<IProductTypeRequest>) {
        try {
            const productType = await ProductTypeApi.save(request.data);
            commit('updateProductType', productType);
            if (request.successCb) {
                request.successCb();
            }
        } catch (error) {
            console.error(error);
            if (request.errorCb) {
                request.errorCb(error.message);
            }
        }
    },
    async updateProductType({commit}, request: IUpdate<IProductTypeRequest>) {
        try {
            const productType = await ProductTypeApi.update(request.id, request.data);
            commit('updateProductType', productType);
            if (request.successCb) {
                request.successCb();
            }
        } catch (error) {
            console.error(error);
            if (request.errorCb) {
                request.errorCb(error.message);
            }
        }
    },
    async deleteProductType({commit}, request: IDelete) {
        try {
            const success = await ProductTypeApi.delete(request.id);
            if (success) {
                commit('deleteProductType', request.id);
                if (request.successCb) {
                    request.successCb();
                }
            } else {
                if (request.errorCb) {
                    request.errorCb('Error calling API');
                }
            }
        } catch (error) {
            console.error(error);
            if (request.errorCb) {
                request.errorCb(error.message);
            }
        }
    },
};
