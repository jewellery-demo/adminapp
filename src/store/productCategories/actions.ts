import {ActionTree} from 'vuex';
import {CrudApi} from '@/backend/Crud';
import {EBackendPath} from '@/typings/enum/backend.enum';
import { ICreate, IDelete, IProductCategory, IRootState, IState, IUpdate } from '@/typings/store';
import { IProductCategoryRequest } from '@/typings/backend';

const ProductCategoryApi = new CrudApi<IProductCategoryRequest, IProductCategory>(EBackendPath.productCategories);

export const actions: ActionTree<IState<IProductCategory>, IRootState> = {
    async loadProductCategories({commit, state}) {
        if (!state.isLoad) {
            const productCategories = await ProductCategoryApi.list();
            if (productCategories && productCategories.length > 0) {
                if (state && state.list.length > 0) {
                    commit('appendProductCategories', productCategories);
                } else {
                    commit('setProductCategories', productCategories);
                }

                commit('setProductCategoriesLoad', true);
            } else {
                commit('setProductCategoriesLoad', true);
            }
        }
    },
    async createProductCategory({commit}, request: ICreate<IProductCategoryRequest>) {
        try {
            const productCategory = await ProductCategoryApi.save(request.data);
            commit('updateProductCategory', productCategory);
            if (request.successCb) {
                request.successCb();
            }
        } catch (error) {
            console.error(error);
            if (request.errorCb) {
                request.errorCb(error.message);
            }
        }
    },
    async updateProductCategory({commit}, request: IUpdate<IProductCategoryRequest>) {
        try {
            const productCategory = await ProductCategoryApi.update(request.id, request.data);
            commit('updateProductCategory', productCategory);
            if (request.successCb) {
                request.successCb();
            }
        } catch (error) {
            console.error(error);
            if (request.errorCb) {
                request.errorCb(error.message);
            }
        }
    },
    async deleteProductCategory({commit}, request: IDelete) {
        try {
            const success = await ProductCategoryApi.delete(request.id);
            if (success) {
                commit('deleteProductCategory', request.id);
                if (request.successCb) {
                    request.successCb();
                }
            } else {
                if (request.errorCb) {
                    request.errorCb('Error calling API');
                }
            }
        } catch (error) {
            console.error(error);
            if (request.errorCb) {
                request.errorCb(error.message);
            }
        }
    },
};
