import {Module} from 'vuex';
import {getters} from '@/store/productCategories/getters';
import {actions} from '@/store/productCategories/actions';
import {mutations} from '@/store/productCategories/mutations';
import { IProductCategory, IRootState, IState } from '@/typings/store';

export const state: IState<IProductCategory> = {
    list: [],
    isLoad: false,
};

const productCategories: Module<IState<IProductCategory>, IRootState> = {
    state,
    getters,
    actions,
    mutations,
};

export default productCategories;
