import {MutationTree} from 'vuex';
import { IProductCategory, IState } from '@/typings/store';

let timeout: number;
export const mutations: MutationTree<IState<IProductCategory>> = {
    setProductCategories(state: IState<IProductCategory>, data: IProductCategory[]) {
        state.list = [...data];
    },
    appendProductCategories(state: IState<IProductCategory>, data: IProductCategory[]) {
        const addList = data.filter((category) => !state.list
            .some((stateCategory) => stateCategory.id === category.id));
        state.list = [...state.list, ...addList];
    },
    setProductCategoriesLoad(state: IState<IProductCategory>, isLoad: boolean) {
        state.isLoad = isLoad;
        if (state.isLoad) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(() => {
                state.isLoad = false;
            }, 60000);
        }
    },
    updateProductCategory(state: IState<IProductCategory>, data: IProductCategory) {
        if (state.list.some((category) => category.id === data.id)) {
            state.list = state.list.map((category) => {
                if (category.id === data.id) {
                    category.label = data.label;
                    category.code = data.code;
                    category.createdAt = data.createdAt;
                    category.updatedAt = data.updatedAt;
                }
                return category;
            });
        } else {
            state.list.push({...data});
        }
    },
    deleteProductCategory(state: IState<IProductCategory>, id: string) {
        state.list = state.list.filter((category) => category.id !== id);
    },
};
