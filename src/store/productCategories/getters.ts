import {GetterTree} from 'vuex';
import { IProductCategory, IRootState, IState } from '@/typings/store';

export const getters: GetterTree<IState<IProductCategory>, IRootState> = {
    productCategories(state): IProductCategory[] {
        return [...state.list];
    },
    productCategory(state): (id: string) => IProductCategory | undefined {
        return (id: string) => state.list.find((c) => c.id === id);
    },
    isProductCategoriesLoad(state): boolean {
        return state.isLoad;
    },
};
