import axios, { AxiosResponse } from 'axios';
import { env } from '@/constants/environment';
import { ISearchCount } from '@/typings/store';

export class SearchCountsApi {
    private readonly path: string = 'search/counts';

    public async list(): Promise<ISearchCount[]> {
        try {
            const response: AxiosResponse = await axios.get<ISearchCount[]>(`${env.API_URL}/${this.path}`);
            if (response.status === 200) {
                return response.data;
            } else {
                return [];
            }
        } catch (error) {
            console.error('list error: ', error);
            return [];
        }
    }

    public async addCount(): Promise<ISearchCount | undefined> {
        try {
            const response: AxiosResponse = await axios.post(`${env.API_URL}/${this.path}`);
            if (response.status === 200) {
                return response.data;
            } else {
                return undefined;
            }
        } catch (error) {
            console.error('addCount error: ', error);
            return undefined;
        }
    }
}
