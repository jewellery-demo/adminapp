import {env} from '@/constants/environment';
import axios, {AxiosResponse} from 'axios';

export class CrudApi<R, S> {
    private readonly path: string;

    constructor(path: string) {
        this.path = path;
    }

    public async list(): Promise<S[]> {
        try {
            const response: AxiosResponse = await axios.get<S[]>(`${env.API_URL}/${this.path}`);
            if (response.status === 200) {
                return response.data;
            } else {
                return [];
            }
        } catch (error) {
            console.error('list error: ', error);
            return [];
        }
    }

    public async get(id: string): Promise<S | undefined> {
        try {
            const response: AxiosResponse = await axios.get<S>(`${env.API_URL}/${this.path}/${id}`);
            if (response.status === 200) {
                return response.data;
            } else {
                return undefined;
            }
        } catch (error) {
            console.error('get error: ', error);
            return undefined;
        }
    }

    public async delete(id: string): Promise<boolean> {
        try {
            const response: AxiosResponse = await axios.delete(`${env.API_URL}/${this.path}/${id}`);
            if (response.status === 200) {
                return true;
            } else {
                return false;
            }
        } catch (error) {
            console.error('delete error: ', error);
            if (error.response && error.response.data && error.response.data.message) {
                throw Error(error.response.data.message);
            } else {
                throw error;
            }
        }
    }

    public async save(request: R): Promise<S | undefined> {
        try {
            const response: AxiosResponse = await axios.put<S>(`${env.API_URL}/${this.path}`, request);
            if (response.status === 200) {
                return response.data;
            } else {
                return undefined;
            }
        } catch (error) {
            console.error('save error: ', error);
            return undefined;
        }
    }

    public async update(id: string, request: R): Promise<S | undefined> {
        try {
            const response: AxiosResponse = await axios.post<S>(`${env.API_URL}/${this.path}/${id}`, request);
            if (response.status === 200) {
                return response.data;
            } else {
                return undefined;
            }
        } catch (error) {
            console.error('update error: ', error);
            return undefined;
        }
    }
}
