import {required} from 'vuelidate/lib/validators';

export const validations = {
    code: {
        required,
    },
    label: {
        required,
    },
};
