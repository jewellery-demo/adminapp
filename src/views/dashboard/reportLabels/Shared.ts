import {required} from 'vuelidate/lib/validators';

export const validations = {
    label: {
        required,
    },
};
