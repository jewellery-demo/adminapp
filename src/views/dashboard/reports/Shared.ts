import {minLength, required} from 'vuelidate/lib/validators';

export const validations = {
    name: {
        required,
    },
    photo: {
        required,
    },
    disclosure: {
        required,
    },
    signatures: {
        required,
    },
    productCategoryId: {
        required,
    },
    productTypeId: {
        required,
    },
    reportFields: {
        required,
        minLength: minLength(1),
    },
};
