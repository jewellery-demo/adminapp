// Views - static
import Login from '@/views/static/Login.vue';
import Register from '@/views/static/Register.vue';

export const loginRoute = {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
        anonymous: true,
    },
};

export const registerRoute = {
    path: '/register',
    name: 'Register',
    component: Register,
    meta: {
        anonymous: true,
    },
};
