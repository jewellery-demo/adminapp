import Widgets from '@/views/dashboard/Widgets.vue';

export const widgetsRoute = {
    path: 'widgets',
    name: 'Widgets',
    component: Widgets,
};
