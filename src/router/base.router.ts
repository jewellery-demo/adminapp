import Cards from '@/views/dashboard/base/Cards.vue';
import Forms from '@/views/dashboard/base/Forms.vue';
import Switches from '@/views/dashboard/base/Switches.vue';
import Tables from '@/views/dashboard/base/Tables.vue';
import Tabs from '@/views/dashboard/base/Tabs.vue';
import Breadcrumbs from '@/views/dashboard/base/Breadcrumbs.vue';
import Carousels from '@/views/dashboard/base/Carousels.vue';
import Collapses from '@/views/dashboard/base/Collapses.vue';
import Jumbotrons from '@/views/dashboard/base/Jumbotrons.vue';
import ListGroups from '@/views/dashboard/base/ListGroups.vue';
import Navs from '@/views/dashboard/base/Navs.vue';
import Navbars from '@/views/dashboard/base/Navbars.vue';
import Paginations from '@/views/dashboard/base/Paginations.vue';
import Popovers from '@/views/dashboard/base/Popovers.vue';
import ProgressBars from '@/views/dashboard/base/ProgressBars.vue';
import Tooltips from '@/views/dashboard/base/Tooltips.vue';

export const baseRoute = {
    path: 'base',
    redirect: '/base/cards',
    name: 'Base',
    component: {
        render(c: any) { return c('router-view'); },
    },
    children: [
        {
            path: 'cards',
            name: 'Cards',
            component: Cards,
        },
        {
            path: 'forms',
            name: 'Forms',
            component: Forms,
        },
        {
            path: 'switches',
            name: 'Switches',
            component: Switches,
        },
        {
            path: 'tables',
            name: 'Tables',
            component: Tables,
        },
        {
            path: 'tabs',
            name: 'Tabs',
            component: Tabs,
        },
        {
            path: 'breadcrumbs',
            name: 'Breadcrumbs',
            component: Breadcrumbs,
        },
        {
            path: 'carousels',
            name: 'Carousels',
            component: Carousels,
        },
        {
            path: 'collapses',
            name: 'Collapses',
            component: Collapses,
        },
        {
            path: 'jumbotrons',
            name: 'Jumbotrons',
            component: Jumbotrons,
        },
        {
            path: 'list-groups',
            name: 'List Groups',
            component: ListGroups,
        },
        {
            path: 'navs',
            name: 'Navs',
            component: Navs,
        },
        {
            path: 'navbars',
            name: 'Navbars',
            component: Navbars,
        },
        {
            path: 'paginations',
            name: 'Paginations',
            component: Paginations,
        },
        {
            path: 'popovers',
            name: 'Popovers',
            component: Popovers,
        },
        {
            path: 'progress-bars',
            name: 'Progress Bars',
            component: ProgressBars,
        },
        {
            path: 'tooltips',
            name: 'Tooltips',
            component: Tooltips,
        },
    ],
};
