import Alerts from '@/views/dashboard/notifications/Alerts.vue';
import Badges from '@/views/dashboard/notifications/Badges.vue';
import Modals from '@/views/dashboard/notifications/Modals.vue';

export const notificationsRoute = {
    path: 'notifications',
    redirect: '/notifications/alerts',
    name: 'Notifications',
    component: {
        render(c: any) { return c('router-view'); },
    },
    children: [
        {
            path: 'alerts',
            name: 'Alerts',
            component: Alerts,
        },
        {
            path: 'badges',
            name: 'Badges',
            component: Badges,
        },
        {
            path: 'modals',
            name: 'Modals',
            component: Modals,
        },
    ],
};
