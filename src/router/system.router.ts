// Views - system
import Page404 from '@/views/system/Page404.vue';
import Page500 from '@/views/system/Page500.vue';

export const systemRoute = {
    path: '/system',
    redirect: '/system/404',
    name: 'System',
    component: {
        render(c: any) { return c('router-view'); },
    },
    children: [
        {
            path: '404',
            name: 'Page404',
            component: Page404,
        },
        {
            path: '500',
            name: 'Page500',
            component: Page500,
        },
    ],
};
