import StandardButtons from '@/views/dashboard/buttons/StandardButtons.vue';
import ButtonGroups from '@/views/dashboard/buttons/ButtonGroups.vue';
import Dropdowns from '@/views/dashboard/buttons/Dropdowns.vue';
import BrandButtons from '@/views/dashboard/buttons/BrandButtons.vue';

export const buttonsRoute = {
    path: 'buttons',
    redirect: '/buttons/standard-buttons',
    name: 'Buttons',
    component: {
        render(c: any) { return c('router-view'); },
    },
    children: [
        {
            path: 'standard-buttons',
            name: 'Standard Buttons',
            component: StandardButtons,
        },
        {
            path: 'button-groups',
            name: 'Button Groups',
            component: ButtonGroups,
        },
        {
            path: 'dropdowns',
            name: 'Dropdowns',
            component: Dropdowns,
        },
        {
            path: 'brand-buttons',
            name: 'Brand Buttons',
            component: BrandButtons,
        },
    ],
};
