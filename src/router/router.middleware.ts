import { setUser } from '@/shared/security';

export const authenticate = async (to: any, from: any, next: any) => {
    let userInfo: IUserInfo | undefined = JSON.parse(localStorage.getItem('userInfo') as string);
    if (userInfo && (Date.now() - new Date(userInfo.createdDate).getTime()) >= 3600000) {
        localStorage.removeItem('userInfo');
        userInfo = undefined;
    }
    setUser(userInfo);

    if (to.matched.some((record: IRouterRecord) => record.meta.auth)) {
        if (userInfo) {
            if (to.matched
                .some((record: IRouterRecord) => record.meta.roles && record.meta.roles.length > 0)) {
                if (to.matched
                    .every((record: IRouterRecord) => !record.meta.roles ||
                        record.meta.roles.some((role) => userInfo!.roles.indexOf(role) >= 0))) {
                    next();
                } else {
                    next({name: 'Home'});
                }
            } else {
                next();
            }
        } else {
            next({name: 'Login'});
        }
    } else if (to.matched.some((record: IRouterRecord) => record.meta.anonymous)) {
        if (userInfo) {
            next({name: 'Home'});
        } else {
            next();
        }
    } else {
        next();
    }
};
