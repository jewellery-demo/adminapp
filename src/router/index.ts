import Vue from 'vue';
import Router from 'vue-router';
import {dashboardRoute} from '@/router/dashboard.router';
import {systemRoute} from '@/router/system.router';
import {loginRoute, registerRoute} from '@/router/static.rounter';
import { authenticate } from '@/router/router.middleware';

Vue.use(Router);

const router = new Router({
    mode: 'hash',
    base: process.env.BASE_URL,
    routes: [
        dashboardRoute,
        loginRoute,
        registerRoute,
        systemRoute,
    ],
});

router.beforeEach(authenticate);

export default router;
