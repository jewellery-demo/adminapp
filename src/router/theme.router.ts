import Colors from '@/views/dashboard/theme/Colors.vue';
import Typography from '@/views/dashboard/theme/Typography.vue';

export const themeRoute = {
    path: 'theme',
    redirect: '/theme/colors',
    name: 'Theme',
    component: {
        render(c: any) { return c('router-view'); },
    },
    children: [
        {
            path: 'colors',
            name: 'Colors',
            component: Colors,
        },
        {
            path: 'typography',
            name: 'Typography',
            component: Typography,
        },
    ],
};
