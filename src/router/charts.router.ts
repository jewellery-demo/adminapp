import Charts from '@/views/dashboard/Charts.vue';

export const chartsRoute = {
    path: 'charts',
    name: 'Charts',
    component: Charts,
};
