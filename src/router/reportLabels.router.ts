import List from '@/views/dashboard/reportLabels/List.vue';
import Detail from '@/views/dashboard/reportLabels/Detail.vue';
import Create from '@/views/dashboard/reportLabels/Create.vue';

export const reportLabelsRoute = {
    path: '/reportLabels',
    meta: { label: 'Report Fields', auth: true, roles: ['admin']},
    component: {
        render(c: any) { return c('router-view'); },
    },
    children: [
        {
            path: '',
            component: List,
        },
        {
            path: '/reportLabels/create',
            meta: { label: 'Create Report Label'},
            name: 'CreateReportLabel',
            component: Create,
        },
        {
            path: ':id',
            meta: { label: 'Report Label Details'},
            name: 'ReportLabel',
            component: Detail,
        },
    ],
};
