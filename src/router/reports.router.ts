import List from '@/views/dashboard/reports/List.vue';
import Detail from '@/views/dashboard/reports/Detail.vue';
import Create from '@/views/dashboard/reports/Create.vue';

export const reportsRoute = {
    path: '/reports',
    meta: { label: 'Reports'},
    component: {
        render(c: any) { return c('router-view'); },
    },
    children: [
        {
            path: '',
            component: List,
            meta: { auth: true, roles: ['admin']},
        },
        {
            path: '/reports/create',
            meta: { label: 'Create Report', auth: true, roles: ['admin']},
            name: 'CreateReport',
            component: Create,

        },
        {
            path: ':id',
            meta: { label: 'Report Details'},
            name: 'Report',
            component: Detail,
        },
    ],
};
