import List from '@/views/dashboard/productCategories/List.vue';
import Detail from '@/views/dashboard/productCategories/Detail.vue';
import Create from '@/views/dashboard/productCategories/Create.vue';

export const productCategoriesRoute = {
    path: '/productCategories',
    meta: { label: 'Product Categories', auth: true, roles: ['admin']},
    component: {
        render(c: any) { return c('router-view'); },
    },
    children: [
        {
            path: '',
            component: List,
        },
        {
            path: '/productCategories/create',
            meta: { label: 'Create Product Category'},
            name: 'CreateProductCategory',
            component: Create,
        },
        {
            path: ':id',
            meta: { label: 'Product Category Details'},
            name: 'ProductCategory',
            component: Detail,
        },
    ],
};
