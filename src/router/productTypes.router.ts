import List from '@/views/dashboard/productTypes/List.vue';
import Detail from '@/views/dashboard/productTypes/Detail.vue';
import Create from '@/views/dashboard/productTypes/Create.vue';

export const productTypesRoute = {
    path: '/productTypes',
    meta: { label: 'Product Types', auth: true, roles: ['admin']},
    component: {
        render(c: any) { return c('router-view'); },
    },
    children: [
        {
            path: '',
            component: List,
        },
        {
            path: '/productTypes/create',
            meta: { label: 'Create Product Type'},
            name: 'CreateProductType',
            component: Create,
        },
        {
            path: ':id',
            meta: { label: 'Product Type Details'},
            name: 'ProductType',
            component: Detail,
        },
    ],
};
