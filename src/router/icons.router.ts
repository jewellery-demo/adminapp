import Flags from '@/views/dashboard/icons/Flags.vue';
import FontAwesome from '@/views/dashboard/icons/FontAwesome.vue';
import SimpleLineIcons from '@/views/dashboard/icons/SimpleLineIcons.vue';
import CoreUIIcons from '@/views/dashboard/icons/CoreUIIcons.vue';

export const iconsRoute = {
    path: 'icons',
    redirect: '/icons/font-awesome',
    name: 'Icons',
    component: {
        render(c: any) { return c('router-view'); },
    },
    children: [
        {
            path: 'coreui-icons',
            name: 'CoreUI Icons',
            component: CoreUIIcons,
        },
        {
            path: 'flags',
            name: 'Flags',
            component: Flags,
        },
        {
            path: 'font-awesome',
            name: 'Font Awesome',
            component: FontAwesome,
        },
        {
            path: 'simple-line-icons',
            name: 'Simple Line Icons',
            component: SimpleLineIcons,
        },
    ],
};
