// Containers
import DefaultContainer from '@/views/components/DefaultContainer.vue';

// Views
import Dashboard from '@/views/dashboard/tabs/Dashboard.vue';
import Home from '@/views/dashboard/tabs/Home.vue';
import Admin from '@/views/dashboard/tabs/Admin.vue';

import {themeRoute} from '@/router/theme.router';
import {chartsRoute} from '@/router/charts.router';
import {widgetsRoute} from '@/router/widgets.router';
import {usersRoute} from '@/router/users.router';
import {baseRoute} from '@/router/base.router';
import {buttonsRoute} from '@/router/buttons.router';
import {iconsRoute} from '@/router/icons.router';
import {notificationsRoute} from '@/router/notifications.router';
import {productCategoriesRoute} from '@/router/productCategories.router';
import {productTypesRoute} from '@/router/productTypes.router';
import {reportLabelsRoute} from '@/router/reportLabels.router';
import {reportsRoute} from '@/router/reports.router';

export const dashboardRoute = {
    path: '/',
    redirect: '/home',
    name: 'Home',
    component: DefaultContainer,
    children: [
        {
            path: 'home',
            component: Home,
        },
        {
            path: 'admin',
            name: 'Admin',
            component: Admin,
        },
        {
            path: 'dashboard',
            name: 'Dashboard',
            component: Dashboard,
            meta: {
                auth: true,
                roles: ['admin'],
            },
        },
        themeRoute,
        chartsRoute,
        widgetsRoute,
        usersRoute,
        baseRoute,
        buttonsRoute,
        iconsRoute,
        notificationsRoute,
        productCategoriesRoute,
        productTypesRoute,
        reportLabelsRoute,
        reportsRoute,
    ],
};
