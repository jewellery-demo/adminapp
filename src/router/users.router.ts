import Users from '@/views/dashboard/users/Users.vue';
import User from '@/views/dashboard/users/User.vue';

export const usersRoute = {
    path: 'users',
    meta: { label: 'Users'},
    component: {
        render(c: any) { return c('router-view'); },
    },
    children: [
        {
            path: '',
            component: Users,
        },
        {
            path: ':id',
            meta: { label: 'User Details'},
            name: 'User',
            component: User,
        },
    ],
};
