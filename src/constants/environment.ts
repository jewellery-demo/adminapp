export const env = (process.env.NODE_ENV === 'production') ?
{
    API_URL: 'https://igrl-gemlab.com/api',
} : {
    API_URL: 'http://localhost:8080',
};
